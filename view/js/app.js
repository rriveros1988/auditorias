/* Funciones de apoyo */
function esconderMenu(){
  $("#logoLinkWeb").hide();
  $("#menu-lateral").css("width","45px");
  $("#menu-lateral").css("background","rgba(30, 0, 0, 0.0)");
  $("#logoMenu").css("color","black");
  $("#iconoLogoMenu").css("border","1px solid #b5b5b5");
  $("#iconoLogoMenu").css("background","rgba(255, 255, 255, 1.0)");
  if($("#sesionActiva").val() == 1){
    $("#lineaMenu").fadeOut();
    $.ajax({
      url:   'controller/datosAreas.php',
      type:  'post',
      success: function (response2) {
        var p2 = jQuery.parseJSON(response2);
        console.log(p2);
        if(p2.aaData.length !== 0){
          for(var i = 0; i < p2.aaData.length; i++){
            $('div[id *=' + p2.aaData[i].PADRE + ']').fadeOut();
            $('li[id *=' + p2.aaData[i].NOMBRE + ']').fadeOut();
          }
        }
      }
    });
  }
  $("#iconoLogoMenu").attr("class","imgMenu fas fas fa-bars");
  $("#logoMenu").fadeIn();
}

var app = angular.module("WPApp", ["ngRoute"]);

app.config(function($routeProvider, $locationProvider) {
    $routeProvider
    .when("/home", {
        controller: "homeController",
        controllerAs: "vm",
        templateUrl : "view/home/home.html"
    })
    .when("/logged", {
        controller: "loggedController",
        controllerAs: "vm",
        templateUrl : "view/home/logged.html"
    })
    .when("/nuevoAST", {
        controller: "nuevoAstController",
        controllerAs: "vm",
        templateUrl : "view/ast/nuevoAst.html"
    })
    .otherwise({redirectTo: '/home'});

    $locationProvider.hashPrefix('');
});

app.controller("homeController", function(){
    $("#modalAlertasSplash").modal({backdrop: 'static', keyboard: false});
    $("#textoModalSplash").html("<img src='view/img/loading.gif' class='splash_charge_logo'><font style='font-size: 12pt;'>Cargando</font>");
    $('#modalAlertasSplash').modal('show');
    // $.ajax({
    //   url:   'controller/limpia_session.php',
    //   type:  'post',
    //   success:  function (response) {
    //
    //   }
    // });
    setTimeout(async function(){
        $('#contenido').fadeIn();
        $('#header').fadeIn();
        $('#footer').fadeIn();

        await esconderMenu();
        $("#logoMenu").fadeOut();
        // $('#menu-lateral').hover(function(){
        //     $("#menu-lateral").css("width","200px");
        // },
        // function() {
        //     $("#menu-lateral").css("width","45px");
        // });
        $("#loginSystem").show("slide", {direction: "up"}, 800);
    },1500);
    setTimeout(function(){
      $('#modalAlertasSplash').modal('hide');
    },2000);
});

app.controller("loggedController", function(){
    $("#modalAlertasSplash").modal({backdrop: 'static', keyboard: false});
    $("#textoModalSplash").html("<img src='view/img/loading.gif' class='splash_charge_logo'><font style='font-size: 12pt;'>Cargando</font>");
    $('#modalAlertasSplash').modal('show');
    // $.ajax({
    //   url:   'controller/limpia_session.php',
    //   type:  'post',
    //   success:  function (response) {
    //
    //   }
    // });
    setTimeout(function(){
      $('#contenido').fadeIn();
      $('#header').fadeIn();
      $('#footer').fadeIn();

      esconderMenu();
      // $('#menu-lateral').hover(function(){
      //     $("#menu-lateral").css("width","200px");
      // },
      // function() {
      //     $("#menu-lateral").css("width","45px");
      // });
    },1500);
    setTimeout(function(){
      $('#modalAlertasSplash').modal('hide');
    },2000);
});

app.controller("nuevoAstController", function(){
    $("#modalAlertasSplash").modal({backdrop: 'static', keyboard: false});
    $("#textoModalSplash").html("<img src='view/img/loading.gif' class='splash_charge_logo'><font style='font-size: 12pt;'>Cargando</font>");
    $('#modalAlertasSplash').modal('show');
    // $.ajax({
    //   url:   'controller/limpia_session.php',
    //   type:  'post',
    //   success:  function (response) {
    //
    //   }
    // });
    setTimeout(function(){
      $('#contenido').fadeIn();
      $('#header').fadeIn();
      $('#footer').fadeIn();

      esconderMenu();
      // $('#menu-lateral').hover(function(){
      //     $("#menu-lateral").css("width","200px");
      // },
      // function() {
      //     $("#menu-lateral").css("width","45px");
      // });

      $("#tipoActividadAst").select2({
          theme: "bootstrap"
      });
    },1500);
    setTimeout(function(){
      $('#modalAlertasSplash').modal('hide');
    },2000);
});
