//Recarga
// if( /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) ) {
//   window.onunload = window.onbeforeunload = function () {
//     $.ajax({
//         url:   'controller/cerraSesion.php',
//         type:  'post',
//         success: function (response) {
//           $.ajax({
//             url:   'controller/datosAreas.php',
//             type:  'post',
//             success: function (response2) {
//               var p2 = jQuery.parseJSON(response2);
//               if(p2.aaData.length !== 0){
//                 for(var i = 0; i < p2.aaData.length; i++){
//                   $('div[id *=' + p2.aaData[i].PADRE + ']').css("display","none");
//                   $('li[id *=' + p2.aaData[i].NOMBRE + ']').css("display","none");
//                 }
//                 window.location.href = "#/home";
//                 $("#logoLinkWeb").fadeOut();
//                 $("#logoMenu").fadeOut();
//                 $("#lineaMenu").fadeOut();
//                 $("#iconoLogoMenu").attr("class","imgMenu fas fas fa-bars");
//                 $("#menu-lateral").css("width","45px");
//                 $("#menu-lateral").css("background","rgba(30, 0, 0, 0.0)");
//                 $("#logoMenu").css("color","black");
//                 $("#iconoLogoMenu").css("border","1px solid #b5b5b5");
//                 $("#iconoLogoMenu").css("background","rgba(255, 255, 255, 1.0)");
//                 if($("#sesionActiva").val() == 0){
//                   if($("#sesionActivaUso").val() == 0){
//                     $("#cerradoInactivo").css("display","none");
//                   }
//                   else{
//                     $("#cerradoInactivo").css("display","block");
//                   }
//                 }
//                 else{
//                   $("#cerradoInactivo").css("display","block");
//                   $("#sesionActiva").val("0");
//                   $("#sesionActivaUso").val("1");
//                 }
//               }
//             }
//           });
//         }
//     });
//   }
// }
// else{
//   window.addEventListener("beforeunload", function (event) {
//     $.ajax({
//         url:   'controller/cerraSesion.php',
//         type:  'post',
//         success: function (response) {
//           $.ajax({
//             url:   'controller/datosAreas.php',
//             type:  'post',
//             success: function (response2) {
//               var p2 = jQuery.parseJSON(response2);
//               if(p2.aaData.length !== 0){
//                 for(var i = 0; i < p2.aaData.length; i++){
//                   $('div[id *=' + p2.aaData[i].PADRE + ']').css("display","none");
//                   $('li[id *=' + p2.aaData[i].NOMBRE + ']').css("display","none");
//                 }
//                 window.location.href = "#/home";
//                 $("#logoLinkWeb").fadeOut();
//                 $("#logoMenu").fadeOut();
//                 $("#lineaMenu").fadeOut();
//                 $("#iconoLogoMenu").attr("class","imgMenu fas fas fa-bars");
//                 $("#menu-lateral").css("width","45px");
//                 $("#menu-lateral").css("background","rgba(30, 0, 0, 0.0)");
//                 $("#logoMenu").css("color","black");
//                 $("#iconoLogoMenu").css("border","1px solid #b5b5b5");
//                 $("#iconoLogoMenu").css("background","rgba(255, 255, 255, 1.0)");
//                 if($("#sesionActiva").val() == 0){
//                   if($("#sesionActivaUso").val() == 0){
//                     $("#cerradoInactivo").css("display","none");
//                   }
//                   else{
//                     $("#cerradoInactivo").css("display","block");
//                   }
//                 }
//                 else{
//                   $("#cerradoInactivo").css("display","block");
//                   $("#sesionActiva").val("0");
//                   $("#sesionActivaUso").val("1");
//                 }
//               }
//             }
//           });
//         }
//     });
//   });
// }
$(window).on("load",function(event){
  event.preventDefault();
  event.stopImmediatePropagation();
  $.ajax({
      url:   'controller/checkToken.php',
      type:  'post',
      success: function (response) {
        if(response === 'TOKEN_NO'){
          $(".contenedor-logos").css("display","none");
          $(".contenedor-logos").find('li').css("display","none");
          window.location.href = "#/home";
        }
        else{
          $.ajax({
              url:   'controller/datosRefresh.php',
              type:  'post',
              success: function (response) {
                var p = jQuery.parseJSON(response);
                var size = Object.size(p.aaData)/2;
                if(size > 0){
                  if(p.aaData['ESTADO'] === 'Activo'){
                    n = p.aaData['NOMBRE'].split(" ");
                    if(n.length <= 3){
                      $("#nombrePerfil").html(p.aaData['NOMBRE']);
                    }
                    else{
                      $("#nombrePerfil").html(n[0] + ' ' + n[2] + ' ' + n[3]);
                    }
                    $.ajax({
                      url:   'controller/datosAreasComunes.php',
                      type:  'post',
                      success: function (response2) {
                        var p2 = jQuery.parseJSON(response2);
                        if(p2.aaData.length !== 0){
                          $(".contenedor-logos").css("display","none");
                          $(".contenedor-logos").find('li').css("display","none");
                          $("#sesionActiva").val("1");
                          $("#sesionActivaUso").val("0");
                          $("#logoMenu").fadeIn();
                        }
                      }
                    });
                  }
                  else{
                    $(".contenedor-logos").css("display","none");
                    $(".contenedor-logos").find('li').css("display","none");
                    window.location.href = "#/home";
                  }
                }
                else{
                  $(".contenedor-logos").css("display","none");
                  $(".contenedor-logos").find('li').css("display","none");
                  window.location.href = "#/home";
                }
              }
          });
        }
      },
      complete: function(){
        $('#contenido').fadeIn();
        $('#footer').fadeIn();
        $('#menu-lateral').fadeIn();
      }
  });
  setInterval(function(){
    if($("#sesionActiva").val() != "0"){
      $.ajax({
            url:   'controller/checkToken.php',
            type:  'post',
            success: function (response) {
              if(response === 'TOKEN_NO'){
                $.ajax({
                    url:   'controller/cerraSesion.php',
                    type:  'post',
                    success: function (response) {
                      $.ajax({
                        url:   'controller/datosAreas.php',
                        type:  'post',
                        success: function (response2) {
                          var p2 = jQuery.parseJSON(response2);
                          if(p2.aaData.length !== 0){
                            for(var i = 0; i < p2.aaData.length; i++){
                              $('div[id *=' + p2.aaData[i].PADRE + ']').css("display","none");
                              $('li[id *=' + p2.aaData[i].NOMBRE + ']').css("display","none");
                            }
                            window.location.href = "#/home";
                            $("#logoLinkWeb").fadeOut();
                               $("#logoMenu").fadeOut();
                               $("#lineaMenu").fadeOut();
                               $("#iconoLogoMenu").attr("class","imgMenu fas fas fa-bars");
                            $("#menu-lateral").css("width","45px");
                            $("#menu-lateral").css("background","rgba(30, 0, 0, 0.0)");
                            $("#logoMenu").css("color","black");
                            $("#iconoLogoMenu").css("border","1px solid #b5b5b5");
                            $("#iconoLogoMenu").css("background","rgba(255, 255, 255, 1.0)");
                            if($("#sesionActiva").val() == 0){
                              if($("#sesionActivaUso").val() == 0){
                                $("#cerradoInactivo").css("display","none");
                              }
                              else{
                                $("#cerradoInactivo").css("display","block");
                              }
                            }
                            else{
                              $("#cerradoInactivo").css("display","block");
                              $("#sesionActiva").val("0");
                              $("#sesionActivaUso").val("1");
                            }
                          }
                        }
                      });
                    }
                });
              }
            }
        });
    }
  },20000);
});

//Login a sistema
$("#loginSystem-submit").unbind('click').click(function(){
    var URLactual = window.location;
    var parametros = {
        "pass" : $("#loginSystem-pass").val(),
        "rut" : $("#loginSystem-rut").val().replace('.','').replace('.',''),
        "url" : URLactual.toString()
    };
    $.ajax({
        data:  parametros,
        url:   'controller/datosUsuarioConectado.php',
        type:  'post',
        beforeSend: function(){
            $("#modalAlertasSplash").modal({backdrop: 'static', keyboard: false});
            $("#textoModalSplash").html("<img src='view/img/loading.gif' class='splash_charge_logo'><font style='font-size: 12pt;'>Cargando</font>");
            $('#modalAlertasSplash').modal('show');
        },
        success: function (response) {
          var p = jQuery.parseJSON(response);
          if(p.aaData.length !== 0){
            if(p.aaData[0]['CHECK'] == 'NO' || $("#loginSystem-rut").val().replace('.','').replace('.','') == '13913245-9'){
              $.ajax({
                  data:  parametros,
                  url:   'controller/datosLogin.php',
                  type:  'post',
                  beforeSend: function(){
                      $("#modalAlertasSplash").modal({backdrop: 'static', keyboard: false});
                      $("#textoModalSplash").html("<img src='view/img/loading.gif' class='splash_charge_logo'><font style='font-size: 12pt;'>Cargando</font>");
                      $('#modalAlertasSplash').modal('show');
                  },
                  success: function (response) {
                    var p = jQuery.parseJSON(response);
                    var size = Object.size(p.aaData)/2;
                    if(size > 0){
                      if(p.aaData['ESTADO'] === 'Activo'){
                        n = p.aaData['NOMBRE'].split(" ");
                        if(n.length <= 3){
                          $("#nombrePerfil").html(p.aaData['NOMBRE']);
                        }
                        else{
                          $("#nombrePerfil").html(n[0] + ' ' + n[2] + ' ' + n[3]);
                        }
                        window.location.href = "#/logged";
                        $.ajax({
                          url:   'controller/datosAreasComunesPadres.php',
                          type:  'post',
                          success: function (response2) {
                            var p2 = jQuery.parseJSON(response2);
                            if(p2.aaData.length !== 0){
                              $("#cantMenu").val(p2.aaData.length);
                            }
                          }
                        });
                        $("#sesionActiva").val("1");
                        $("#sesionActivaUso").val("0");
                        setTimeout(function(){
                          $("#logoMenu").fadeIn();
                        },500);
                      }
                      else{
                        $("#buttonAceptarAlerta").css("display","inline");
                        $("#modalAlertas").modal({backdrop: 'static', keyboard: false});
                        $("#textoModal").html("El usuario ingresado no se encuentra activo en sistema");
                        setTimeout(function(){
                          $('#modalAlertasSplash').modal('hide');
                        },500);
                        $('#modalAlertas').modal('show');
                      }
                    }
                    else{
                      $("#buttonAceptarAlerta").css("display","inline");
                      $("#modalAlertas").modal({backdrop: 'static', keyboard: false});
                      $("#textoModal").html("Datos de acceso incorrectos o no registrados en sistema");
                      setTimeout(function(){
                        $('#modalAlertasSplash').modal('hide');
                      },500);
                      $('#modalAlertas').modal('show');
                    }
                  }
              });
            }
            else{
              $("#buttonAceptarAlerta").css("display","inline");
              $("#modalAlertas").modal({backdrop: 'static', keyboard: false});
              $("#textoModal").html("El usuario ingresado ya se encuentra conectado al sistema");
              setTimeout(function(){
                $('#modalAlertasSplash').modal('hide');
              },500);
              $('#modalAlertas').modal('show');
            }
          }
          else{
            $("#buttonAceptarAlerta").css("display","inline");
            $("#modalAlertas").modal({backdrop: 'static', keyboard: false});
            $("#textoModal").html("Datos de acceso incorrectos o no registrados en sistema");
            setTimeout(function(){
              $('#modalAlertasSplash').modal('hide');
            },500);
            $('#modalAlertas').modal('show');
          }
        }
    });
});

//Verificar input quita borde rojo
$("#loginSystem-rut").on('input', function(){
  $(this).css("border","");
});

//Verifica rut
function Rut()
{
  var texto = window.document.getElementById("loginSystem-rut").value;
  var tmpstr = "";
  for ( i=0; i < texto.length ; i++ )
    if ( texto.charAt(i) != ' ' && texto.charAt(i) != '.' && texto.charAt(i) != '-' )
      tmpstr = tmpstr + texto.charAt(i);
  texto = tmpstr;
  largo = texto.length;

  if(texto == ""){
    return false;
  }
  else if ( largo < 2 )
  {
    $("#buttonAceptarAlerta").css("display","inline");
    $("#textoModal").html("Debe ingresar el rut completo");
    $('#modalAlertas').modal('show');
    window.document.getElementById("loginSystem-rut").value = "";
    $("#loginSystem-rut").css("border","1px solid red");
    window.document.getElementById("loginSystem-rut").focus();
    window.document.getElementById("loginSystem-rut").select();
    return false;
  }

  for (i=0; i < largo ; i++ )
  {
    if ( texto.charAt(i) !="0" && texto.charAt(i) != "1" && texto.charAt(i) !="2" && texto.charAt(i) != "3" && texto.charAt(i) != "4" && texto.charAt(i) !="5" && texto.charAt(i) != "6" && texto.charAt(i) != "7" && texto.charAt(i) !="8" && texto.charAt(i) != "9" && texto.charAt(i) !="k" && texto.charAt(i) != "K" )
    {
      $("#buttonAceptarAlerta").css("display","inline");
      $("#textoModal").html("Los datos ingresados no corresponden a un rut válido");
      $('#modalAlertas').modal('show');
      window.document.getElementById("loginSystem-rut").value = "";
      $("#loginSystem-rut").css("border","1px solid red");
      window.document.getElementById("loginSystem-rut").focus();
      window.document.getElementById("loginSystem-rut").select();
      return false;
    }
  }

  var invertido = "";
  for ( i=(largo-1),j=0; i>=0; i--,j++ )
    invertido = invertido + texto.charAt(i);
  var dtexto = "";
  dtexto = dtexto + invertido.charAt(0);
  dtexto = dtexto + '-';
  cnt = 0;

  for ( i=1,j=2; i<largo; i++,j++ )
  {
    //alert("i=[" + i + "] j=[" + j +"]" );
    if ( cnt == 3 )
    {
      dtexto = dtexto + '.';
      j++;
      dtexto = dtexto + invertido.charAt(i);
      cnt = 1;
    }
    else
    {
      dtexto = dtexto + invertido.charAt(i);
      cnt++;
    }
  }

  invertido = "";
  for ( i=(dtexto.length-1),j=0; i>=0; i--,j++ )
    invertido = invertido + dtexto.charAt(i);

  window.document.getElementById("loginSystem-rut").value = invertido.toUpperCase()

  if ( revisarDigito(texto) )
    return true;

  return false;
}

function revisarDigito2( dvr )
{
  dv = dvr + ""
  if ( dv != "" && dv != '0' && dv != '1' && dv != '2' && dv != '3' && dv != '4' && dv != '5' && dv != '6' && dv != '7' && dv != '8' && dv != '9' && dv != 'k'  && dv != 'K')
  {
    $("#buttonAceptarAlerta").css("display","inline");
    $("#textoModal").html("Debe ingresar un digito verificador válido");
    $('#modalAlertas').modal('show');
    $("#loginSystem-rut").css("border","1px solid red");
    window.document.getElementById("loginSystem-rut").focus();
    window.document.getElementById("loginSystem-rut").select();
    return false;
  }
  return true;
}

function revisarDigito( crut )
{
  largo = crut.length;
  if(crut == ""){
    return false;
  }
  else if ( largo < 2)
  {
    $("#buttonAceptarAlerta").css("display","inline");
    $("#textoModal").html("Debe ingresar el rut completo");
    $('#modalAlertas').modal('show');
    window.document.getElementById("loginSystem-rut").value = "";
    $("#loginSystem-rut").css("border","1px solid red");
    window.document.getElementById("loginSystem-rut").focus();
    window.document.getElementById("loginSystem-rut").select();
    return false;
  }
  if ( largo > 2 )
    rut = crut.substring(0, largo - 1);
  else
    rut = crut.charAt(0);
  dv = crut.charAt(largo-1);
  revisarDigito2( dv );

  if ( rut == null || dv == null )
    return 0

  var dvr = '0'
  suma = 0
  mul  = 2

  for (i= rut.length -1 ; i >= 0; i--)
  {
    suma = suma + rut.charAt(i) * mul
    if (mul == 7)
      mul = 2
    else
      mul++
  }
  res = suma % 11
  if (res==1)
    dvr = 'k'
  else if (res==0)
    dvr = '0'
  else
  {
    dvi = 11-res
    dvr = dvi + ""
  }
  if ( dvr != dv.toLowerCase() )
  {
    $("#buttonAceptarAlerta").css("display","inline");
    $("#textoModal").html("Los datos ingresados no corresponden a un rut válido");
    $('#modalAlertas').modal('show');
    window.document.getElementById("loginSystem-rut").value = "";
    $("#loginSystem-rut").css("border","1px solid red");
    window.document.getElementById("loginSystem-rut").focus();
    window.document.getElementById("loginSystem-rut").select();
    return false
  }

  return true
}

Object.size = function(obj) {
    var size = 0, key;
    for (key in obj) {
        if (obj.hasOwnProperty(key)) size++;
    }
    return size;
};

//Cerrar sesion
$("#buttonLogout").unbind('click').click(function(){
  $.ajax({
      url:   'controller/cerraSesion.php',
      type:  'post',
      success: function (response) {
        $.ajax({
          url:   'controller/datosAreas.php',
          type:  'post',
          success: function (response2) {
            var p2 = jQuery.parseJSON(response2);
            if(p2.aaData.length !== 0){
              for(var i = 0; i < p2.aaData.length; i++){
                $('div[id *=' + p2.aaData[i].PADRE + ']').css("display","none");
                $('li[id *=' + p2.aaData[i].NOMBRE + ']').css("display","none");
              }
              $("#sesionActiva").val("0");
              $("#sesionActivaUso").val("0");
              window.location.href = "#/home";
              $("#logoLinkWeb").fadeOut();
              $("#logoMenu").fadeOut();
              $("#lineaMenu").fadeOut();
              $("#iconoLogoMenu").attr("class","imgMenu fas fas fa-bars");
              $("#menu-lateral").css("width","45px");
              $("#menu-lateral").css("background","rgba(30, 0, 0, 0.0)");
              $("#logoMenu").css("color","black");
              $("#iconoLogoMenu").css("border","1px solid #b5b5b5");
              $("#iconoLogoMenu").css("background","rgba(255, 255, 255, 1.0)");
            }
          }
        });
      }
  });
});
