<!-- <script>
	function randomTextoLoadJs(){
	  var str = '';
	  var ref = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRST0123456789';
	  for (var i = 0; i < 8 ; i++)
	  {
	    str += ref.charAt(Math.floor(Math.random()*ref.length));
	  }
	  return str;
	}
  var js = document.createElement('script');
  js.src = 'view/js/funciones.js?idLoad=' + randomTextoLoadJs();
  document.getElementsByTagName('head')[0].appendChild(js);
</script> -->

<!-- Modal de alertas -->
<div id="modalAlertasSplash" class="modal modal-fullscreen fade" role="dialog" style="z-index: 1800;">
  <div class="modal-dialog" role="document">

    <!-- Modal content-->
    <div class="modal-content-t">
      <div class="modal-body alerta-modal-body">
        <h4 id="textoModalSplash"></h4>
        <button id="buttonAceptarAlertaSplash" style="margin-top: 10px; display: none;" type="button" class="btn btn-primary" data-dismiss="modal">Aceptar</button>
        <button id="botonAceptarCambioPassSplash" style="margin-top: 10px; display: none;" type="button" class="btn btn-primary" data-dismiss="modal">Aceptar</button>
      </div>
    </div>

  </div>
</div>

<!-- Modal de alertas -->
<div id="modalAlertas" class="modal modal-fullscreen fade" role="dialog" style="z-index: 1800;">
  <div class="modal-dialog modal-dialog-box" role="document">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-body alerta-modal-body">
        <h6 id="textoModal"></h6>
        <button id="buttonAceptarAlerta" style="margin-top: 10px; display: none;" type="button" class="btn btn-primary" data-dismiss="modal">Aceptar</button>
        <button id="botonAceptarCambioPass" style="margin-top: 10px; display: none;" type="button" class="btn btn-primary" data-dismiss="modal">Aceptar</button>
      </div>
    </div>

  </div>
</div>

<!-- Termometro -->
<div id="modalTieneTermometro" class="modal modal-fullscreen fade" role="dialog">
  <div class="modal-dialog modal-lg modal-dialog-box">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title"><i class="fas fa-thermometer-half"></i>&nbsp;&nbsp;Asignación de termómetro</h5>
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>
      <div class="modal-body alerta-modal-body">
        <div class="row" style="text-align: left;">
          <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <h5 id="textoModal">Favor indicar si termómetro fue entregado a:<br><br>
              DNI: <span id="dniTermometro"></span><br>
              Nombre: <span id="nombreTermometro"></span>
            </h5>
          </div>
        </div>
      </div>
      <div class="modal-footer">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
          <button id="botonGuardarSi" style="margin-top: 5px; margin-bottom: 10px;" type="button" class="btn btn-danger">Si</button>
          <button id="botonGuardarNo" style="margin-top: 5px; margin-bottom: 10px; margin-left: 15px;" type="button" class="btn btn-danger">No</button>
        </div>
      </div>
    </div>
  </div>
</div>
