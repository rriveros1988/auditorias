<?php
  // ini_set('display_errors', 'On');
  require('../model/consultas.php');
  session_start();
  setcookie("tk_w_o",$_COOKIE["tk_w_o"],time()+300);
  actualizaTokenLogin($_SESSION['rutUser'], $_COOKIE["tk_w_o"]);

	if(count($_POST) >= 0){
        $rutUser = $_SESSION['rutUser'];
        $permisos = consultaAreasComunes($rutUser);

        $permisoPersonal = 0;
        for($i = 0; $i < count($permisos); $i++){
          if($permisos[$i]['NOMBRE'] == 'buttonMiPersonal'){
            $permisoPersonal = $permisos[$i]['TODOS'];
          }
        }

        if($permisoPersonal == 1){
            $row = consultaDiasCantidadTodos();
        }
        else{
            $row = consultaDiasCantidad($rutUser);
        }

        if(is_array($row))
        {
            $results = array(
                "sEcho" => 1,
                "iTotalRecords" => count($row),
                "iTotalDisplayRecords" => count($row),
                "aaData"=>$row
            );
            echo json_encode($results);
        }
        else{
            $results = array(
                "sEcho" => 1,
                "iTotalRecords" => 0,
                "iTotalDisplayRecords" => 0,
                "aaData"=>[]
            );
            echo json_encode($results);
        }
	}
	else{
		echo "Sin datos";
	}
?>
