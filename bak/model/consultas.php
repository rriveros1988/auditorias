<?php
	 // ini_set('display_errors', 'On');
	require('conexion.php');

	//Login
	function consultaUsuarioConectado($rut){
		$con = conectar();
		if($con != 'No conectado'){
			$sql = "SELECT
CASE WHEN TIMESTAMPDIFF(SECOND , TOKEN_WEB_TIME, NOW()) < 300 THEN 'SI' ELSE 'NO' END 'CHECK'
FROM USUARIO
WHERE RUT = '{$rut}'";
			if ($row = $con->query($sql)) {
				$return = array();
				while($array = $row->fetch_array(MYSQLI_BOTH)){
					$return[] = $array;
				}

				return $return;
			}
			else{
				return "Error";
			}
		}
		else{
			return "Error";
		}
	}

	function checkUsuario($rut, $pass){
		$con = conectar();
		if($con != 'No conectado'){
			$sql = "SELECT NOMBRE, RUT, IDPERFIL, ESTADO
			FROM USUARIO
			WHERE RUT = '" . $rut . "' AND PASS = '" . $pass . "'";
			if ($row = $con->query($sql)) {

				$array = $row->fetch_array(MYSQLI_BOTH);

				return $array;
			}
			else{
				return "Error";
			}
		}
		else{
			return "Error";
		}
	}

	//Refresh
	function checkUsuarioSinPass($rut){
		$con = conectar();
		if($con != 'No conectado'){
			$sql = "SELECT NOMBRE, RUT, IDPERFIL, ESTADO
			FROM USUARIO
			WHERE RUT = '" . $rut . "'";
			if ($row = $con->query($sql)) {

				$array = $row->fetch_array(MYSQLI_BOTH);

				return $array;
			}
			else{
				return "Error";
			}
		}
		else{
			return "Error";
		}
	}

	//Ingresa Token Login
	function actualizaTokenLogin($rut, $token){
		$con = conectar();
		$con->query("START TRANSACTION");
		if($con != 'No conectado'){
			$sql = "UPDATE USUARIO
SET TOKEN_WEB = '{$token}',
TOKEN_WEB_TIME = NOW()
WHERE RUT = '{$rut}'";
			if ($con->query($sql)) {
			    $con->query("COMMIT");
			    return "Ok";
			}
			else{
				$con->query("ROLLBACK");
				return "Error";
			}
		}
		else{
			$con->query("ROLLBACK");
			return "Error";
		}
	}

	//Chequeo Token
	function checkToken($token){
		$con = conectar();
		if($con != 'No conectado'){
			$sql = "SELECT RUT
FROM USUARIO
WHERE TOKEN_WEB  = '{$token}'";
			if ($row = $con->query($sql)) {

				$array = $row->fetch_array(MYSQLI_BOTH);

				return $array;
			}
			else{
				return "Error";
			}
		}
		else{
			return "Error";
		}
	}

	//Borra Token Login
	function borraTokenLogin($rut){
		$con = conectar();
		$con->query("START TRANSACTION");
		if($con != 'No conectado'){
			$sql = "UPDATE USUARIO
SET TOKEN_WEB = NULL
WHERE RUT = '{$rut}'";
			if ($con->query($sql)) {
			    $con->query("COMMIT");
			    return "Ok";
			}
			else{
				$con->query("ROLLBACK");
				return "Error";
			}
		}
		else{
			$con->query("ROLLBACK");
			return "Error";
		}
	}

	//Areas padres
	function consultaAreasComunesPadre($rut){
		$con = conectar();
		if($con != 'No conectado'){
			$sql = "SELECT DISTINCT PADRE
FROM AREAWEB
WHERE PADRE IN
(
SELECT A.PADRE
FROM AREAWEB A
LEFT JOIN PERMISOS P
ON A.IDAREAWEB = P.IDAREAWEB
LEFT JOIN USUARIO U
ON P.IDUSUARIO = U.IDUSUARIO
WHERE A.TIPO = 0
AND U.RUT = '{$rut}'
)
UNIOn ALL
SELECT DISTINCT PADRE
FROM AREAWEB
WHERE TIPO = 1";
			if ($row = $con->query($sql)) {
				$return = array();
				while($array = $row->fetch_array(MYSQLI_BOTH)){
					$return[] = $array;
				}

				return $return;
			}
			else{
				return "Error";
			}
		}
		else{
			return "Error";
		}
	}

	//Areas comunes
	function consultaAreasComunes(){
		$con = conectar();
		if($con != 'No conectado'){
			$sql = "SELECT NOMBRE, PADRE, TIPO
	FROM AREAWEB
	WHERE TIPO = 1";
			if ($row = $con->query($sql)) {
				$return = array();
				while($array = $row->fetch_array(MYSQLI_BOTH)){
					$return[] = $array;
				}

				return $return;
			}
			else{
				return "Error";
			}
		}
		else{
			return "Error";
		}
	}

	//Areas comunes
	function consultaAreas(){
		$con = conectar();
		if($con != 'No conectado'){
			$sql = "SELECT NOMBRE, PADRE, TIPO
	FROM AREAWEB";
			if ($row = $con->query($sql)) {
				$return = array();
				while($array = $row->fetch_array(MYSQLI_BOTH)){
					$return[] = $array;
				}

				return $return;
			}
			else{
				return "Error";
			}
		}
		else{
			return "Error";
		}
	}

	//Destruye Token Login
	function destruyeTokenLogin($rut){
		$con = conectar();
		$con->query("START TRANSACTION");
		if($con != 'No conectado'){
			$sql = "UPDATE USUARIO
SET TOKEN_WEB = NULL,
TOKEN_WEB_TIME = NULL
WHERE RUT = '{$rut}'";
			if ($con->query($sql)) {
			    $con->query("COMMIT");
			    return "Ok";
			}
			else{
				$con->query("ROLLBACK");
				return "Error";
			}
		}
		else{
			$con->query("ROLLBACK");
			return "Error";
		}
	}

?>
