var app = angular.module("WPApp", ["ngRoute"]);

app.config(function($routeProvider, $locationProvider) {
    $routeProvider
    .when("/home", {
        controller: "homeController",
        controllerAs: "vm",
        templateUrl : "view/home/home.html"
    })
    .when("/personal", {
        controller: "homeController",
        controllerAs: "vm",
        templateUrl : "view/home/logged.html"
    })
    .when("/nuevoAST", {
        controller: "nuevoAstController",
        controllerAs: "vm",
        templateUrl : "view/ast/nuevoAst.html"
    })
    .otherwise({redirectTo: '/home'});

    $locationProvider.hashPrefix('');
});

app.controller("homeController", function(){
    $("#modalAlertasSplash").modal({backdrop: 'static', keyboard: false});
    $("#textoModalSplash").html("<img src='view/img/loading.gif' class='splash_charge_logo'><font style='font-size: 12pt;'>Cargando</font>");
    $('#modalAlertasSplash').modal('show');
    // $.ajax({
    //   url:   'controller/limpia_session.php',
    //   type:  'post',
    //   success:  function (response) {
    //
    //   }
    // });
    setTimeout(function(){
        $('#contenido').fadeIn();
        $('#header').fadeIn();
        $('#footer').fadeIn();
        $('#menu-lateral').fadeIn();
        $('#menu-lateral').hover(function(){
            $("#menu-lateral").css("width","200px");
        },
        function() {
            $("#menu-lateral").css("width","45px");
        });
        $("#loginSystem").show("slide", {direction: "up"}, 800);
    },1500);
    setTimeout(function(){
      $('#modalAlertasSplash').modal('hide');
    },2000);
});

app.controller("loggedController", function(){
    $("#modalAlertasSplash").modal({backdrop: 'static', keyboard: false});
    $("#textoModalSplash").html("<img src='view/img/loading.gif' class='splash_charge_logo'><font style='font-size: 12pt;'>Cargando</font>");
    $('#modalAlertasSplash').modal('show');
    // $.ajax({
    //   url:   'controller/limpia_session.php',
    //   type:  'post',
    //   success:  function (response) {
    //
    //   }
    // });
    setTimeout(function(){
      $('#contenido').fadeIn();
      $('#header').fadeIn();
      $('#footer').fadeIn();
      $('#menu-lateral').fadeIn();
      $('#menu-lateral').hover(function(){
          $("#menu-lateral").css("width","200px");
      },
      function() {
          $("#menu-lateral").css("width","45px");
      });
    },1500);
    setTimeout(function(){
      $('#modalAlertasSplash').modal('hide');
    },2000);
});

app.controller("nuevoAstController", function(){
    $("#modalAlertasSplash").modal({backdrop: 'static', keyboard: false});
    $("#textoModalSplash").html("<img src='view/img/loading.gif' class='splash_charge_logo'><font style='font-size: 12pt;'>Cargando</font>");
    $('#modalAlertasSplash').modal('show');
    // $.ajax({
    //   url:   'controller/limpia_session.php',
    //   type:  'post',
    //   success:  function (response) {
    //
    //   }
    // });
    setTimeout(function(){
      $('#contenido').fadeIn();
      $('#header').fadeIn();
      $('#footer').fadeIn();
      $('#menu-lateral').fadeIn();
      $('#menu-lateral').hover(function(){
          $("#menu-lateral").css("width","200px");
      },
      function() {
          $("#menu-lateral").css("width","45px");
      });

      $("#tipoActividadAst").select2({
          theme: "bootstrap"
      });
    },1500);
    setTimeout(function(){
      $('#modalAlertasSplash').modal('hide');
    },2000);
});
